package com.scansione.pdfshower.helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2016 Karmick Solution Pvt. Ltd.
 */
public class MarshmallowPermissionHelper {
    public static int REQUEST_LOCATION_SERVICE = 121;
    //Request code for Location services
    public static final int LOCATION_PERMISSION_REQUEST = 101;
    //Request code for Enable GPS
    public static final int ENABLE_GPS_REQUEST = 102;

    /* For GPS or Location permission checking from Activity */
    public static boolean isLocationPermission(final Activity mActivity, final int REQUEST_CODE) {
        Logger.setLogTag("LOCATION_PERMISSION_REQUEST");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (null != mActivity) {
                if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat
                            .shouldShowRequestPermissionRationale(mActivity,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        Logger.showInfoLog("*** Need to show explanation ***");

                        Map<String, String> map = new HashMap<>();
                        map.put(Alerts.DialogKeys.TITLE, "Alert!");
                        map.put(Alerts.DialogKeys.MESSAGE, "You need to grant access to get your current location.");
                        map.put(Alerts.DialogKeys.NATURAL_BUTTON_TEXT, "Got it");
                        Alerts.singleButtonAlert(mActivity, map, new Alerts.SingleButtonAlert() {
                            @TargetApi(Build.VERSION_CODES.M)
                            @Override
                            public void onNaturalButtonClick(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                                mActivity.requestPermissions(new String[]{
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                            }
                        });
                        /*Popups.showOkPopup(AppUtils.getStringFromResource(R.string.permission_location)
                                , mActivity, new Popups.CustomDialogCallback() {
                                    @TargetApi(Build.VERSION_CODES.M)
                                    @Override
                                    public void onOkClick() {
                                        mActivity.requestPermissions(new String[]{
                                                Manifest.permission.ACCESS_FINE_LOCATION,
                                                Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                                    }
                                });*/
                        return false;
                    } else {
                        Logger.showInfoLog("*** No need to show explanation ***");
                        mActivity.requestPermissions(new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }

        } else {
            return true;
        }
    }

    /* For GPS or Location permission checking from Fragment */
    public static boolean isLocationPermission(final Activity mActivity, final Fragment mFragment,
                                               final int REQUEST_CODE) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (null != mActivity) {
                if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat
                            .shouldShowRequestPermissionRationale(mActivity,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                        Map<String, String> map = new HashMap<>();
                        map.put(Alerts.DialogKeys.TITLE, "Alert!");
                        map.put(Alerts.DialogKeys.MESSAGE, "You need to grant access to get your current location.");
                        map.put(Alerts.DialogKeys.NATURAL_BUTTON_TEXT, "Got it");
                        Alerts.singleButtonAlert(mActivity, map, new Alerts.SingleButtonAlert() {
                            @TargetApi(Build.VERSION_CODES.M)
                            @Override
                            public void onNaturalButtonClick(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                                if (null != mFragment) {
                                    mFragment.requestPermissions(new String[]{
                                            Manifest.permission.ACCESS_FINE_LOCATION,
                                            Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                                } else {
                                    mActivity.requestPermissions(new String[]{
                                            Manifest.permission.ACCESS_FINE_LOCATION,
                                            Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                                }
                            }
                        });

                        /*Popups.showOkPopup(AppUtils.getStringFromResource(R.string.permission_location)
                                , mActivity, new Popups.CustomDialogCallback() {
                                    @TargetApi(Build.VERSION_CODES.M)
                                    @Override
                                    public void onOkClick() {
                                        if (null != mFragment) {
                                            mFragment.requestPermissions(new String[]{
                                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                                    Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                                        } else {
                                            mActivity.requestPermissions(new String[]{
                                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                                    Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                                        }
                                    }
                                });*/
                        return false;
                    } else {
                        if (null != mFragment) {
                            mFragment.requestPermissions(new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                        } else {
                            mActivity.requestPermissions(new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                        }
                        return false;
                    }

                } else {
                    return true;
                }
            } else {
                return false;
            }

        } else {
            return true;
        }
    }

    /* Alert user to enable GPS to get there locations */
    public static boolean checkDeviceLocationSetting(final Activity mActivity,
                                                     final Alerts.TwoButtonAlert callback) {
        if (null != mActivity) {
            if (!isLocationSwitchEnabled(mActivity)) {
                /**
                 * Ask user to enable location service, to get user current location.
                 */
                Map<String, String> map = new HashMap<>();
                map.put(Alerts.DialogKeys.TITLE, "Location Service Alert!");
                map.put(Alerts.DialogKeys.MESSAGE, "Location service is not enabled in your device." +
                        " Please enable location switch(GPS) to get your current location.");
                map.put(Alerts.DialogKeys.POSITIVE_BUTTON_TEXT, "Open location setting");
                map.put(Alerts.DialogKeys.NEGATIVE_BUTTON_TEXT, "Cancel");
                Alerts.twoButtonAlert(mActivity, map, callback);

                /*Popups.showTwoButtonPopup(AppUtils.getStringFromResource(
                        R.string.gps_network_not_enabled), mActivity,
                        AppUtils.getStringFromResource(R.string.open_location_settings),
                        AppUtils.getStringFromResource(R.string.cancel),
                        new Popups.YesNoClickListener() {
                            @Override
                            public void onYesButtonClick() {
                                if (null != callback) {
                                    *//*Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    mActivity.startActivityForResult(myIntent, ENABLE_GPS_REQUEST);*//*
                                    callback.onPositiveButtonClick();
                                }
                            }

                            @Override
                            public void onNoButtonClick() {
                                if (null != callback) {
                                    callback.onNegativeButtonClick();
                                }
                            }
                        });*/

                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /* Check location hardware (Location) switch */

    public static boolean isLocationSwitchEnabled(final Context mContext) {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            Logger.printStackTrace(ex);
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            Logger.printStackTrace(ex);
        }
        return gps_enabled && network_enabled;
    }

    public interface PositiveNegativeButtonClickListener {
        void onPositiveButtonClick();

        void onNegativeButtonClick();
    }

    /*Permission Checking for Storage And Camera*/
    public static boolean getStorageAndCameraPermission(final Context context,
                                                        final Fragment fragment,
                                                        final Activity activity,
                                                        final int REQUEST_CODE) {
        Logger.showErrorLog("GET STORAGE PERMISSION");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Logger.showErrorLog("ITS M");

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                Logger.showErrorLog("CURRENT ACTIVITY");

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    Logger.showErrorLog("SHOULD I SHOW EXPLANATION");
                    Map<String, String> map = new HashMap<>();
                    map.put(Alerts.DialogKeys.TITLE, "Alert!");
                    map.put(Alerts.DialogKeys.MESSAGE, "You need to grant access to use this feature");
                    map.put(Alerts.DialogKeys.NATURAL_BUTTON_TEXT, "Got it");
                    Alerts.singleButtonAlert(activity, map, new Alerts.SingleButtonAlert() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onNaturalButtonClick(DialogInterface dialogInterface) {
                            dialogInterface.dismiss();
                            if (null != fragment) {
                                fragment.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
                            } else {
                                activity.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
                            }
                        }
                    });


                    return false;
                } else {
                    if (fragment != null) {
                        fragment.requestPermissions(
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CODE);

                    } else if (null != activity) {
                        activity.requestPermissions(
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CODE);
                    }


                }
                return false;
            } else
                return true;
        } else
            return true;


    }
}
