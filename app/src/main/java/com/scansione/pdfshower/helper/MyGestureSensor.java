package com.scansione.pdfshower.helper;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created on 16-Mar-16.
 */
public class MyGestureSensor extends GestureDetector.SimpleOnGestureListener {
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    SwipeGesture _callBack;

    public MyGestureSensor(SwipeGesture _callBack) {
        this._callBack = _callBack;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {


        try {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH ||
                    Math.abs(e2.getY() - e1.getY()) > SWIPE_MAX_OFF_PATH) {
                return false;
            }
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE &&
                    Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                /*Left Swipe*/
                _callBack.onLeftSwipe();

            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE &&
                    Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                /*Right Swipe*/
                _callBack.onRightSwipe();
            }
        } catch (Exception e) {
            Logger.printStackTrace(e);
        }


        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return super.onDown(e);
    }

    public interface SwipeGesture {
        void onLeftSwipe();

        void onRightSwipe();
    }
}
