package com.scansione.pdfshower.helper;

/**
 * Created by ajijul on 20/12/16.
 */

public class Constant {
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 212;
    public static final int PDF_LOADER_ID = 101;
}
