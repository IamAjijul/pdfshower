package com.scansione.pdfshower.helper;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


import com.scansione.pdfshower.R;

import java.util.Map;

/**
 * Copyright (C) Karmick Solution Pvt. Ltd. on 26/8/16.
 */
public class Alerts {

    public static void twoButtonAlert(Activity mActivity, Map<String, String> map,
                                      final TwoButtonAlert twoButtonAlertCallback) {
        AlertDialog alertDialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.MyAlertDialogStyle);
        builder.setTitle(map.get(DialogKeys.TITLE));
        builder.setMessage(map.get(DialogKeys.MESSAGE));
        builder.setPositiveButton(map.get(DialogKeys.POSITIVE_BUTTON_TEXT), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (null != twoButtonAlertCallback) {
                    twoButtonAlertCallback.onPositiveButtonClick(dialogInterface);
                }
            }
        });
        builder.setNegativeButton(map.get(DialogKeys.NEGATIVE_BUTTON_TEXT), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (null != twoButtonAlertCallback) {
                    twoButtonAlertCallback.onNegativeButtonClick(dialogInterface);
                }
            }
        });
        alertDialog = builder.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }

    public static void singleButtonAlert(Activity mActivity, Map<String, String> map,
                                         final SingleButtonAlert singleButtonAlertCallback) {
        AlertDialog alertDialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.MyAlertDialogStyle);
        builder.setTitle(map.get(DialogKeys.TITLE));
        builder.setMessage(map.get(DialogKeys.MESSAGE));
        builder.setNeutralButton(map.get(DialogKeys.NATURAL_BUTTON_TEXT), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (null != singleButtonAlertCallback) {
                    singleButtonAlertCallback.onNaturalButtonClick(dialogInterface);
                }
            }
        });
        alertDialog = builder.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }

    public interface TwoButtonAlert {
        void onPositiveButtonClick(DialogInterface dialogInterface);

        void onNegativeButtonClick(DialogInterface dialogInterface);
    }

    public interface SingleButtonAlert {
        void onNaturalButtonClick(DialogInterface dialogInterface);
    }

    public interface DialogKeys {
        String TITLE = "title";
        String MESSAGE = "message";
        String POSITIVE_BUTTON_TEXT = "positive_button_text";
        String NEGATIVE_BUTTON_TEXT = "negative_button_text";
        String NATURAL_BUTTON_TEXT = "natural_button_text";
    }
}
