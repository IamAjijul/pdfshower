package com.scansione.pdfshower;

import android.app.Application;

import io.paperdb.Paper;

/**
 * Created by ajijul on 23/1/17.
 */

public class ApplicationClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(this);
    }
}
