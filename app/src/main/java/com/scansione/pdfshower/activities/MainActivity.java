package com.scansione.pdfshower.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.MimeTypeMap;

import com.scansione.pdfshower.R;
import com.scansione.pdfshower.helper.Constant;
import com.scansione.pdfshower.helper.Logger;
import com.scansione.pdfshower.helper.MarshmallowPermissionHelper;

import java.io.File;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (MarshmallowPermissionHelper.getStorageAndCameraPermission(this, null, this,
                Constant.STORAGE_PERMISSION_REQUEST_CODE)) {
            initClassVariables();
        }
    }

    private void initClassVariables() {
        Intent testIntent = new Intent(Intent.ACTION_GET_CONTENT);
        testIntent.setType("application/pdf");
        startActivityForResult(testIntent, 102);


    }

    public void onGetPdfClick(View view) {
        if (MarshmallowPermissionHelper.getStorageAndCameraPermission(this, null, this,
                Constant.STORAGE_PERMISSION_REQUEST_CODE)) {
            initClassVariables();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = Uri.parse(data.getData().toString());
        Logger.showErrorLog(uri.toString() + "LINK ");
        Intent intent = new Intent(this, PDFViewerActivity.class);
        Bundle bundle = new Bundle();
        intent.putExtra("path", uri.toString());
        startActivity(intent);

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=?";
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf");
            String[] selectionArgsPdf = new String[]{mimeType};
            Cursor allPdfFiles = getContentResolver().query(contentUri, null, selectionMimeType, selectionArgsPdf, null);
           /* // exclude media files, they would be here also.
            String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;
            cursor = context.getContentResolver().query(contentUri, null, selection, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);*/
            allPdfFiles.moveToFirst();
            return allPdfFiles.getString(0);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            initClassVariables();
        }
    }
}
