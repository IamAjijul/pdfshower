package com.scansione.pdfshower.activities;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansione.pdfshower.R;
import com.scansione.pdfshower.helper.Logger;
import com.scansione.pdfshower.helper.MyGestureSensor;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by ajijul on 24/1/17.
 */

public class PDFViewerActivity extends AppCompatActivity implements View.OnTouchListener {
    private ImageView activityViewer_imvPdf;
    private ParcelFileDescriptor parcelFileDescriptor;
    private PdfRenderer pdfRenderer;
    private int currentPage = 0;
    private PdfRenderer.Page mCurrentPage;
    private int totalPage = 0;
    private TextView activityViewer_tvPage;
    private Animation topToBottom, bottomTotop;
    private boolean isRunning = false;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);
        activityViewer_imvPdf = (ImageView) findViewById(R.id.activityViewer_imvPdf);
        activityViewer_tvPage = (TextView) findViewById(R.id.activityViewer_tvPage);
        if (getIntent().getExtras() != null) {
            try {
                Logger.showErrorLog("OnCreate........." + getPath(this, Uri.parse(getIntent().getExtras().getString("path"))));
                initClassVariables(new File(getPath(this, Uri.parse(getIntent().getExtras().getString("path")))));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        gestureDetector = new GestureDetector(this, new MyGestureSensor(new MyGestureSensor.SwipeGesture() {
            @Override
            public void onLeftSwipe() {
                Logger.showErrorLog("onLeftSwipe");

                if (currentPage < totalPage - 1) {
                    currentPage++;
                    showPage(currentPage);
                }
            }

            @Override
            public void onRightSwipe() {
                Logger.showErrorLog("onRightSwipe");

                if (currentPage > 0) {
                    currentPage--;
                    showPage(currentPage);
                }
            }
        }));

        activityViewer_imvPdf.setOnTouchListener(this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Logger.showErrorLog("OnNewIntent........." + intent.getExtras());

    }

    private void initClassVariables(File file) {
        try {
            parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_WRITE);
            pdfRenderer = new PdfRenderer(parcelFileDescriptor);
            totalPage = pdfRenderer.getPageCount();
            showPage(currentPage);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void showPage(int currentPage) {
        topToBottom = AnimationUtils.loadAnimation(this, R.anim.top_to_bottom);
        bottomTotop = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top);
        if (pdfRenderer == null || pdfRenderer.getPageCount() <= currentPage
                || currentPage < 0) {
            return;
        }
        // For closing the current page before opening another one.
        try {
            if (mCurrentPage != null) {
                mCurrentPage.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Open page with specified index
        mCurrentPage = pdfRenderer.openPage(currentPage);
        Bitmap bitmap = Bitmap.createBitmap(mCurrentPage.getWidth(),
                mCurrentPage.getHeight(), Bitmap.Config.ARGB_8888);

        //Pdf page is rendered on Bitmap
        mCurrentPage.render(bitmap, null, null,
                PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        //Set rendered bitmap to ImageView
        activityViewer_imvPdf.setImageBitmap(bitmap);
        if (!isRunning) {
            Logger.showErrorLog("IF.........");

            isRunning = true;
            activityViewer_tvPage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Logger.showErrorLog("Runnablet.........");
                    isRunning = false;
                    activityViewer_tvPage.startAnimation(bottomTotop);
                    activityViewer_tvPage.setVisibility(View.INVISIBLE);
                }
            }, 1000);
        }
        activityViewer_tvPage.setText((currentPage + 1) + "/" + totalPage);
        activityViewer_tvPage.setVisibility(View.VISIBLE);
        activityViewer_tvPage.startAnimation(topToBottom);
    }

    public void onNextClick(View view) {

        if (currentPage < totalPage - 1) {
            currentPage++;
            showPage(currentPage);
        }
    }

    public void onPreviousClick(View view) {
        if (currentPage > 0) {
            currentPage--;
            showPage(currentPage);
        }
    }

    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return gestureDetector.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }
}

